const arr = [1,2,4,5];

function fakeFetch(number) {
return new Promise((resolve, reject) => {
setTimeout(() => resolve(number * 2), 500);
});
}

async function action() {

    var result = 0;
    for(var i = 0; i < arr.length; i++)
    {
        result += arr[i];
        const res = await fakeFetch(result);
        result += res;
    }

    return result;
}

action().then(v => {
    alert(v);
})



